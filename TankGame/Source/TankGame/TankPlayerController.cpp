// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"
#include "TankAimingComponent.h"
#include "Engine/World.h"
#include "Tank.h"


void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay(); //make sure that the beginplay is called
	//auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	//if (!ensure(AimingComponent)) { return; }
	
	//FoundAimingComponent(AimingComponent);
}



void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AimTowardsCrosshair();

}

void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedPlayerTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedPlayerTank)) { return; }

		PossessedPlayerTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnPossesedPlayerTankDeath); //subscribe to tank delegate calling StartSpectatingOnly function)
	}
}

void ATankPlayerController::OnPossesedPlayerTankDeath()
{
	this->StartSpectatingOnly();
}
void ATankPlayerController::AimTowardsCrosshair() 
{
	if (!ensure(GetPawn())) { return; }
	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(AimingComponent)) { return; }
	
	FVector OutHitLocation; //Out Parameter

	bool bGotHitLocation = GetSightRayHitLocation(OutHitLocation);

	if (bGotHitLocation)
	{
		AimingComponent->AimAt(OutHitLocation);
	}
}


//Get World location of linetrace through crosshair, true if hits landscape
bool ATankPlayerController::GetSightRayHitLocation(FVector& OutHitLocation) const
{
	// Find the crosshair position
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);

	auto ScreenLocation = FVector2D(ViewportSizeX * CrossHairXLocation, ViewportSizeY * CrossHairYLocation);

	//"De-project the screen position of the crosshair to a world direction
	FVector LookDirection;
	if (GetLookDirection(ScreenLocation, LookDirection))
	{
		// Line- trace along that LookDirection adn see what we hit (up to LineTraceRange)
		/*FVector HitLocation;*/
		return GetLookVectorHitLocation(OutHitLocation, LookDirection);
		/*OutHitLocation = HitLocation;*/
		
	}
	return false;
}
	
bool ATankPlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const
{
	FVector CameraWorldLocation; // to be discarded
	DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, CameraWorldLocation, LookDirection);
	return true;
}
bool ATankPlayerController::GetLookVectorHitLocation(FVector& OutHitLocation, FVector LookDirection) const
{
	FHitResult HitResult;
	auto StartLocation = PlayerCameraManager->GetCameraLocation();
	auto EndLocation = StartLocation + (LookDirection * LineTraceRange);

	FCollisionQueryParams TraceParams
	(
		FName(TEXT("")),
		false,
		GetPawn()
	);

	if (GetWorld()->LineTraceSingleByChannel(
		HitResult,
		StartLocation,
		EndLocation,
		ECollisionChannel::ECC_Visibility,
		TraceParams))
	{
		OutHitLocation = HitResult.Location;
		return true;
	}
	OutHitLocation = FVector(0);
	return false;
}

