// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"

class UTankAimingComponent;

UCLASS()
class TANKGAME_API ATankAIController : public AAIController
{
	GENERATED_BODY()

private:
    virtual void BeginPlay() override;
    virtual void Tick(float DeltaTime) override;

    float ReloadTimeInSeconds = 2.f;

    UPROPERTY(EditAnywhere, Category = Setup)
    float AcceptanceRadius = 5000.f;
    UTankAimingComponent* AimingComponent = nullptr;

    virtual void SetPawn(APawn* InPawn) override; //called when pawn is possesed

    UFUNCTION()
    void OnPossesedTankDeath();

};
