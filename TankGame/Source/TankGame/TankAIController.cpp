// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "Engine/World.h"
#include "TankAimingComponent.h"
#include "Tank.h" //so we can implemetne on deathc delegate



void ATankAIController::BeginPlay() 
{
	Super::BeginPlay();
}

void ATankAIController::SetPawn(APawn* InPawn) 
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedTank)) { return; }

		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnPossesedTankDeath); //subscribe to tank delegate calling OnPossesedTankDeath function)
	}
}

void ATankAIController::OnPossesedTankDeath()
{
	if(!ensure(GetPawn())){return;}
	GetPawn()->DetachFromControllerPendingDestroy();
}


void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
	auto ControlledTank = GetPawn(); //Cast zamienia pointer GetPawn na Obiekt ATank (chyba TODO dowiedziec sie wiecej)
	
	if (!ensure(PlayerTank && ControlledTank)) { return; }	
	MoveToActor(PlayerTank, AcceptanceRadius);
		 

	//TODO Remove from tick
	AimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();

	if (!ensure(AimingComponent)) { return; }
	AimingComponent->AimAt(PlayerTank->GetActorLocation());


	if (AimingComponent->GetFiringState()==EFiringStatus::Locked || AimingComponent->GetFiringState() == EFiringStatus::Aiming) //TODO should it fire only when it's locked?
	{
		AimingComponent->Fire();//TODO Fire rate is different that player's ?
	}



}

