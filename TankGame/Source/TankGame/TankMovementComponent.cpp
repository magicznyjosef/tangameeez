// Fill out your copyright notice in the Description page of Project Settings.

#include "TankMovementComponent.h"
#include "TankTrack.h"



void UTankMovementComponent::Initialise(UTankTrack* LeftTrackToSet, UTankTrack* RighTrackToSet) 
{ 
	LeftTrack = LeftTrackToSet;
	RightTrack = RighTrackToSet;
}


void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	//No need to call Super as we're replacing the functionalirty
	auto TankTowards = GetOwner()->GetActorForwardVector().GetSafeNormal();
	auto AIForwardIntention = MoveVelocity.GetSafeNormal();

	auto ForwardThrow = FVector::DotProduct(TankTowards, AIForwardIntention);
	IntendMoveForward(ForwardThrow);


	auto RorationToPlayer = FVector::CrossProduct(TankTowards, AIForwardIntention).Z;

	IntendMoveClockwise(RorationToPlayer);


}
//TODO Make a system that will detect the input, which will pause the game if the gamepad is disconnected
//TODO ^ that will block pressing both on gamepad and keybord to double input which causes OP speed
void UTankMovementComponent::IntendMoveForward(float Throw)
{
	if (!LeftTrack || !LeftTrack) { return; }
	LeftTrack->SetThrottle(Throw); //This could be made in blueprint With connecting intend move forward with SetThrottle but is ugly af
	RightTrack->SetThrottle(Throw);// this works as if it was made in BP
}


void UTankMovementComponent::IntendMoveClockwise(float Rotate)
{
	if (!LeftTrack || !LeftTrack) { return; }
	LeftTrack->SetThrottle(Rotate);
	RightTrack->SetThrottle(-Rotate);
}

