// Copyright The King is Dead


#include "TankGameInstance.h"
#include "Blueprint/UserWidget.h"
#include "Tank.h"

UTankGameInstance::UTankGameInstance()
{
	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/UI/MainMenuUI"));

	if (!ensure(MenuBPClass.Class != nullptr)) return;

	MenuClass = MenuBPClass.Class;
}

void UTankGameInstance::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("Found Class %s"), *MenuClass->GetName());
}

void UTankGameInstance::LoadMenu()
{
	if (!ensure(MenuClass != nullptr)) return;

	UUserWidget* Menu = CreateWidget<UUserWidget>(this, MenuClass);

	Menu->AddToViewport();
	
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(Menu->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = true;
}

void UTankGameInstance::Host()
{
	GEngine->AddOnScreenDebugMessage(0, 2.f, FColor::Green, TEXT("Hosting"));
	
	UWorld* World = GetWorld();

	if (!ensure(World != nullptr)) return;
	
	World->ServerTravel("/Game/_Levels/BattleGround?listen");

}

void UTankGameInstance::Join(const FString& Adress)
{
	GEngine->AddOnScreenDebugMessage(0, 2.f, FColor::Green, FString::Printf(TEXT("Joining %s"), *Adress));

	APlayerController* PlayerController = GetFirstLocalPlayerController();

	if (!ensure(PlayerController != nullptr)) return;

	PlayerController->ClientTravel(Adress, ETravelType::TRAVEL_Absolute);


}
