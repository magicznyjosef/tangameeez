// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

/**
 * TankTracked hello tutaj opis customowy mozna pisac
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TANKGAME_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
    
    UFUNCTION(BlueprintCallable, Category = Input)
    void SetThrottle(float Throttle);

    // Max force per track in Newtons
    UPROPERTY(EditDefaultsOnly)
    float TrackMaxDrivingForce = 35000000.f;   //Assum 40t tank 10g acceleration


private:
    UTankTrack();    

    void DriveTruck(float CurrentThrottle);

    TArray<class ASprungWheel*> GetWheels() const;
    
    UPROPERTY(VisibleAnywhere, Category = "Components")
    UStaticMeshComponent* CollisionMeshTrack = nullptr;
};
