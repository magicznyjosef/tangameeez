// Copyright The King is Dead


#include "TankShooterTest.h"

// Sets default values
ATankShooterTest::ATankShooterTest()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATankShooterTest::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATankShooterTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATankShooterTest::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

