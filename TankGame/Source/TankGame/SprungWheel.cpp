// Copyright The King is Dead

#include "SprungWheel.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Components/SphereComponent.h"


// Sets default values
ASprungWheel::ASprungWheel()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;

	MassWheelConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("MassWheelConstraint"));
	SetRootComponent(MassWheelConstraint);

	Axle3 = CreateDefaultSubobject<USphereComponent>(FName("Axle3"));
	Axle3->SetupAttachment(MassWheelConstraint);

	Wheel3 = CreateDefaultSubobject<USphereComponent>(FName("Wheel3"));
	Wheel3->SetupAttachment(Axle3);

	AxleWheelConstaint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("AxleWheelConstaint"));
	AxleWheelConstaint->SetupAttachment(Axle3);
}

// Called when the game starts or when spawned
void ASprungWheel::BeginPlay()
{
	Super::BeginPlay();

	Wheel3->SetNotifyRigidBodyCollision(true);
	Wheel3->OnComponentHit.AddDynamic(this, &ASprungWheel::OnHit);

	SetupConstaint();
}


// Called every frame
void ASprungWheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetWorld()->TickGroup == TG_PostPhysics) 
	{
		TotalForceMagnitudeThisFrame = 0;
	};

}

void ASprungWheel::SetupConstaint()
{
	if (!GetAttachParentActor()) return;

	UPrimitiveComponent* BodyRoot = Cast<UPrimitiveComponent>(GetAttachParentActor()->GetRootComponent());
	if (!BodyRoot) return;

	MassWheelConstraint->SetConstrainedComponents(BodyRoot, NAME_None, Axle3, NAME_None);
	AxleWheelConstaint->SetConstrainedComponents(Axle3, NAME_None, Wheel3, NAME_None);
}

void ASprungWheel::AddDrivingForce(float ForceMagnitude)
{
	TotalForceMagnitudeThisFrame += ForceMagnitude;
}

void ASprungWheel::ApplyForce() 
{
	Wheel3->AddForce(Axle3->GetForwardVector() * TotalForceMagnitudeThisFrame);

}
void ASprungWheel::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ApplyForce();
}