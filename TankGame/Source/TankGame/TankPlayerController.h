// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

class UTankAimingComponent;//forward declaration

UCLASS()
class TANKGAME_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()


protected:
	UFUNCTION(BlueprintImplementableEvent, Category = Setup)
	void FoundAimingComponent(UTankAimingComponent* AimCompRef);

	virtual void BeginPlay() override; //checks in hierarchy >> idzie w gore do klasy AActror danie virtual tutaj nic nie zmieni tylko dla dzieci 
	virtual void Tick(float DeltaTime) override;

	virtual void SetPawn(APawn* InPawn) override; //called when pawn is possesed

private:
	void AimTowardsCrosshair(); //Start the tank movin the barrel so that a shot would it where the crosshair intersects the worlds
	
	//Return na OUT parameter, true if hit landscape
	bool GetSightRayHitLocation(FVector& OutHitLocation) const;
	bool GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const;
	bool GetLookVectorHitLocation(FVector& HitLocation, FVector LookDirection ) const;

	UPROPERTY(EditAnywhere)
	float CrossHairXLocation = .5f;

	UPROPERTY(EditAnywhere)
	float CrossHairYLocation = .3333f;
	
	UPROPERTY(EditAnywhere)
	float LineTraceRange = 1000000.f; //10km

	UFUNCTION()
	void OnPossesedPlayerTankDeath();
};
