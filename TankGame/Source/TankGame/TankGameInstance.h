// Copyright The King is Dead

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TankGameInstance.generated.h"

/**
 * 
 */

class UUserWidget;
UCLASS()
class TANKGAME_API UTankGameInstance : public UGameInstance
{
	GENERATED_BODY()

	UTankGameInstance();

	virtual void Init();

	UFUNCTION(Exec) //allows using via debug command tylda
	void Host();
	
	UFUNCTION(Exec)
	void Join(const FString& Adress);

	UFUNCTION(BlueprintCallable)
	void LoadMenu();


	private:
	TSubclassOf<UUserWidget> MenuClass;
};
