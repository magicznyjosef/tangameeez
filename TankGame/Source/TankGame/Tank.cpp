// Fill out your copyright notice in the Description page of Project Settings.


#include "Tank.h"

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SetReplicates(true);

	//RootPrimitiveTest2 = CreateDefaultSubobject<UPrimitiveComponent>(FName("RootPrimitiveTest2"));
	//SetRootComponent(RootPrimitiveTest2);
}

float ATank::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	int32 DamagePoints = FPlatformMath::RoundToInt(DamageAmount);
	auto DamageToApply = FMath::Clamp(DamagePoints, 0, CurrentHealth);
	
	CurrentHealth -= DamageToApply;
	if (CurrentHealth <= 0)
	{
		OnDeath.Broadcast();
	}
	return DamageToApply;
}

float ATank::GetHeatlhPercent() const
{
	return (float)CurrentHealth / (float)StartingHealth; //(float) to jest cast do floata
}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = StartingHealth;
}

void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{

Super::SetupPlayerInputComponent(PlayerInputComponent);

}


