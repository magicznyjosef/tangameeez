// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Tank.generated.h" //never paste new includes below


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTankDelegate);

UCLASS()
class TANKGAME_API ATank : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATank();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override; //called by the engine when actor damage is dealt

	FTankDelegate OnDeath;


	// Pure == const, cannot be changed
	UFUNCTION(BlueprintPure, Category = "UI")
	float GetHeatlhPercent() const; // to ui
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override; //virtual means that the method can be overriden by ane descendant in the future f.e. player controller


public:	

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// Called to bind functionality to input
	//UPROPERTY(VisibleAnywhere, Category = "Components")
	//UPrimitiveComponent* RootPrimitiveTest2 = nullptr;


	// Edit DefaultsOnly - the value can be only changed in BP and is applied for every tank
private:
	
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 StartingHealth = 100;

	UPROPERTY(VisibleAnywhere, Category = "Heath")
	int32 CurrentHealth;


};
