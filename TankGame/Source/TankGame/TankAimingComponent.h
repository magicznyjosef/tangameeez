// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

class UTankBarrel; // forward declaration to include only in cpp 
class UTankTurretA;
class AProjectile;

UENUM()
enum class EFiringStatus : uint8
{
	Locked,
	Reloading,
	Aiming,
	OutOfAmmo
};


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TANKGAME_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();

	UFUNCTION(BlueprintCallable, Category = Setup)
	void Initialise(UTankBarrel* BarrelToSet, UTankTurretA* TurretToSet);

	UFUNCTION(BlueprintCallable, Category = Setup)
	void Fire();

	void AimAt(FVector HitLocation);

	void MoveBarrelTowards(FVector AimDirection);

	//made to pass the Reload state and to not change it
	EFiringStatus GetFiringState() const;
	
	UFUNCTION(BlueprintCallable, Category = State)
	int32 GetRoundsLeft() const;

	UPROPERTY(EditAnywhere, Category = Firing)
	float LaunchSpeed = 20000; // TODO find sensible default, mb depends on projectile mass


	UPROPERTY(EditAnywhere, Category = Debug)
	bool bDrawDebugVelocity = false;


	UPROPERTY(EditAnywhere, Category = Setup)
	TSubclassOf<AProjectile> ProjectileBlueprint;

	UPROPERTY(EditAnywhere, Category = Firing)
	float ReloadTimeInSeconds = 3.f;

	UTankBarrel* Barrel = nullptr;
	UTankTurretA* Turret = nullptr;

protected:

	UPROPERTY(BlueprintReadOnly, Category = State)
	EFiringStatus FiringState = EFiringStatus::Reloading;

	UPROPERTY(EditAnywhere, Category = Firing)
	int32 RoundsLeft = 3;

private:

	double LastFireTime = 0;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void BeginPlay() override;

	bool IsBarrelMoving();
	FVector AimDirectionVector;


};
